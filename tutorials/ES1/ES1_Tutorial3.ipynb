{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import networkx as nx\n",
    "%matplotlib inline\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 3\n",
    "\n",
    "This tutorial is based on a tutorial shared on https://github.com/CambridgeUniversityPress/FirstCourseNetworkScience.\n",
    "\n",
    "Contents:\n",
    "\n",
    "1. Paths\n",
    "2. Connected components\n",
    "3. Directed paths & components\n",
    "4. Dataset: US air traffic network"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Paths\n",
    "\n",
    "Let's start with a very simple, undirected network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G = nx.Graph()\n",
    "\n",
    "G.add_nodes_from([1,2,3,4])\n",
    "\n",
    "G.add_edges_from([(1,2),(2,3),(1,3),(1,4)])\n",
    "\n",
    "nx.draw(G, with_labels=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Networkx, a *path* is a sequence of edges connecting two nodes (in the lecture we called it a *walk*). In this simple example, we can easily see that there is indeed at least one path that connects nodes 3 and 4. We can verify this with NetworkX:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.has_path(G, 3, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There can be more than one path between two nodes. Again considering nodes 3 and 4, there are two such \"simple\" paths:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list(nx.all_simple_paths(G, 3, 4))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A simple path is one without any cycles (in the lecture we called this a *path*). If we allowed cycles, there would be infinitely many paths because one could always just go around the cycle as many times as desired.\n",
    "\n",
    "We are often most interested in *shortest* paths. In an unweighted network, the shortest path is the one with the fewest edges. We can see that of the two simple paths between nodes 3 and 4, one is shorter than the other. We can get this shortest path with a single NetworkX function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.shortest_path(G, 3, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you only care about the path length, there's a function for that too:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.shortest_path_length(G, 3, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that a path length is defined here by the number of *edges* in the path, not the number of nodes, which implies\n",
    "\n",
    "    nx.shortest_path_length(G, u, v) == len(nx.shortest_path(G, u, v)) - 1\n",
    "    \n",
    "for nodes $u$ and $v$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Connected components\n",
    "\n",
    "In the simple network above, we can see that for *every* pair of nodes, we can find a path connecting them. This is the definition of a *connected* graph. We can check this property for a given graph:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.is_connected(G)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Not every graph is connected:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G = nx.Graph()\n",
    "\n",
    "nx.add_cycle(G, (1,2,3))\n",
    "G.add_edge(4,5)\n",
    "\n",
    "nx.draw(G, with_labels=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.is_connected(G)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And NetworkX will raise an error if you ask for a path between nodes where none exists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.has_path(G, 3, 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [],
   "source": [
    "nx.shortest_path(G, 3, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Visually, we can identify two connected components in our graph. Let's verify this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.number_connected_components(G)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `nx.connected_components()` function takes a graph and returns a list of sets of node names, one such set for each connected component. Verify that the two sets in the following list correspond to the two connected components in the drawing of the graph above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list(nx.connected_components(G))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In case you're not familiar with Python sets, they are collections of items without duplicates. These are useful for collecting node names because node names should be unique. As with other collections, we can get the number of items in a set with the `len` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "components = list(nx.connected_components(G))\n",
    "len(components[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We often care about the largest connected component, which is sometimes referred to as the *core* of the network. We can make use of Python's builtin `max` function in order to obtain the largest connected component. By default, Python's `max` function sorts things in lexicographic (i.e. alphabetical) order, which is not helpful here. We want the maximum connected component when sorted in order of their sizes, so we pass `len` as a key function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "max(nx.connected_components(G), key=len)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While it's often enough to just have the list of node names, sometimes we need the actual subgraph consisting of the largest connected component. One way to get this is to pass the list of node names to the `G.subgraph()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "core_nodes = max(nx.connected_components(G), key=len)\n",
    "core = G.subgraph(core_nodes)\n",
    "\n",
    "nx.draw(core, with_labels=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Those of you using tab-completion will also notice a `nx.connected_component_subgraphs()` function. This can also be used to get the core subgraph but the method shown is more efficient when you only care about the largest connected component."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Directed paths & components\n",
    "\n",
    "Let's extend these ideas about paths and connected components to directed graphs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "D = nx.DiGraph()\n",
    "D.add_edges_from([\n",
    "    (1,2),\n",
    "    (2,3),\n",
    "    (3,2), (3,4), (3,5),\n",
    "    (4,2), (4,5), (4,6),\n",
    "    (5,6),\n",
    "    (6,4),\n",
    "])\n",
    "nx.draw(D, with_labels=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Directed paths\n",
    "\n",
    "We know that in a directed graph, an edge from an arbitrary node $u$ to an arbitrary node $v$ does not imply that an edge exists from $v$ to $u$. Since paths must follow edge direction in directed graphs, the same asymmetry applies for paths. Observe that this graph has a path from 1 to 4, but not in the reverse direction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.has_path(D, 1, 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.has_path(D, 4, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The other NetworkX functions dealing with paths take this asymmetry into account as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.shortest_path(D, 2, 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.shortest_path(D, 5, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since there is no edge from 5 to 3, the shortest path from 5 to 2 cannot simply backtrack the shortest path from 2 to 5 -- it has to go a longer route through nodes 6 and 4."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Directed components"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Directed networks have two kinds of connectivity. *Strongly connected* means that there exists a directed path between every pair of nodes, i.e., that from any node we can get to any other node while following edge directionality. Think of cars on a network of one-way streets: they can't drive against the flow of traffic."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.is_strongly_connected(D)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Weakly connected* means that there exist a path between every pair of nodes, regardless of direction. Think about pedestrians on a network of one-way streets: they walk on the sidewalks so they don't care about the direction of traffic."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx.is_weakly_connected(D)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If a network is strongly connected, it is also weakly connected. The converse is not always true, as seen in this example.\n",
    "\n",
    "The `is_connected` function for undirected graphs will raise an error when given a directed graph."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [],
   "source": [
    "# This will raise an error\n",
    "nx.is_connected(D)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the directed case, instead of `nx.connected_components` we now have `nx.weakly_connected_components` and `nx.strongly_connected_components`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list(nx.weakly_connected_components(D))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list(nx.strongly_connected_components(D))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Dataset: US air traffic network\n",
    "\n",
    "This repository contains several example network datasets. Among these is a network of US air travel routes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G = nx.read_graphml('./datasets/openflights/openflights_usa.graphml.gz')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The nodes in this graph are airports, represented by their [IATA codes](https://en.wikipedia.org/wiki/List_of_airports_by_IATA_code:_A); two nodes are connected with an edge if there is a scheduled flight directly connecting these two airports. We'll assume this graph to be undirected since a flight in one direction usually means there is a return flight.\n",
    "\n",
    "Thus this graph has edges\n",
    "```\n",
    "[('HOM', 'ANC'), ('BGM', 'PHL'), ('BGM', 'IAD'), ...]\n",
    "```\n",
    "where ANC is Anchorage, IAD is Washington Dulles, etc.\n",
    "\n",
    "These nodes also have **attributes** associated with them, containing additional information about the airports:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G.nodes['IND']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Node attributes are stored as a dictionary, so the values can be accessed individually as such:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G.nodes['IND']['name']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's see now how big the network is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "first_airport = list(G.nodes())[0]\n",
    "print(G.nodes[first_airport])\n",
    "\n",
    "print(list(G.neighbors(first_airport)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We want to explore how well connected a certain node is. Let's look for example at the first airport listed in the network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "first_airport = list(G.nodes())[0]\n",
    "print(G.nodes[first_airport])\n",
    "\n",
    "print(list(G.neighbors(first_airport)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, the first listed airport is the Redding Municipal Airport ('RDD'), and it is directly connected only to 'SFO'. One can see that 'RDD' has only one neighbour by counting how many non-zero elements are present on the first row of adjacency matrix of the network (remember that for Pyhton objects, indexes starts from 0)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G_matrix = nx.to_numpy_matrix(G)\n",
    "print(np.count_nonzero(G_matrix[0,:]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we look at direct flights 'RDD' is quite isolated, but if we look at the first row of i-th power of the adjacency matrix, we can find how many airports are conncected to 'RDD' by walks of length i! Let's see this for walks with length ranging from 1 to 5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "power_G = G_matrix.copy()\n",
    "for i in range(5):\n",
    "    i_walks_node0 = power_G[0,:]\n",
    "\n",
    "    print(f'Number of neighbors reachable by a walk of length {i+1}:', np.count_nonzero(i_walks_node0))\n",
    "    print(f'Total number of walks of length {i+1} starting on node 0:', int(i_walks_node0.sum()))\n",
    "    \n",
    "    power_G = power_G @ G_matrix # matrix multiplication"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, out of the 546 airports (including 'RDD'), one can reach 534 of these with exactly 5 flights. We also see that the total number of walks grows extremely rapidly. This is because walks can have cycles.\n",
    "\n",
    "Let's close this little investigation by looking at how many walks of length ranging from 1 to 5 there are between 'RDD' and its neighbour 'SFO'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "index_SFO = np.nonzero(G_matrix[0]) #We know that 'SFO' its the only neighbour of 'RDD'\n",
    "\n",
    "power_G = G_matrix.copy()\n",
    "for i in range(1,6):\n",
    "    print(power_G[0][index_SFO])\n",
    "    power_G = power_G @ G_matrix # matrix multiplication"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# EXERCISE 1\n",
    "\n",
    "Is there a direct flight between Indianapolis and Fairbanks, Alaska (FAI)? A direct flight is one with no intermediate stops."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# EXERCISE 2\n",
    "\n",
    "If I wanted to fly from Indianapolis to Fairbanks, Alaska what would be an itinerary with the fewest number of flights?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# EXERCISE 3\n",
    "By default, NetworkX uses the Dijkstra algorithm to compute shortest paths (you can check the source code [here](https://networkx.org/documentation/stable/_modules/networkx/algorithms/shortest_paths/generic.html#shortest_path)).\n",
    "Below, you will find an implementation of the Dijkstra algorithm (taken from [here](https://gist.github.com/aeged/db5bfda411903ecd89a3ba3cb7791a05)) using a NetworkX graph and a Python [PriorityQueue](https://docs.python.org/3/library/queue.html).\n",
    "\n",
    "Carefully read it and identify the algorithm's steps given in the lecture's slides."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# dependencies for our dijkstra's implementation\n",
    "from queue import PriorityQueue\n",
    "from math import inf\n",
    "# graph dependency  \n",
    "import networkx as nx\n",
    "\n",
    "\n",
    "\"\"\"Dijkstra's shortest path algorithm\"\"\"\n",
    "def dijkstra(graph: 'networkx.classes.graph.Graph', start: str, end: str) -> 'List':\n",
    "    \"\"\"Get the shortest path of nodes by going backwards through prev list\n",
    "    credits: https://github.com/blkrt/dijkstra-python/blob/3dfeaa789e013567cd1d55c9a4db659309dea7a5/dijkstra.py#L5-L10\"\"\"\n",
    "    def backtrace(prev, start, end):\n",
    "        node = end\n",
    "        path = []\n",
    "        while node != start:\n",
    "            path.append(node)\n",
    "            node = prev[node]\n",
    "        path.append(node) \n",
    "        path.reverse()\n",
    "        return path\n",
    "        \n",
    "    \"\"\"get the cost of edges from node -> node\"\"\"\n",
    "    def cost(u, v):\n",
    "        return 1 # here we consider each edge with a unit length.\n",
    "        \n",
    "    \"\"\"main algorithm\"\"\"\n",
    "    # predecessor of current node on shortest path \n",
    "    prev = {} \n",
    "    # initialize distances from start -> given node i.e. dist[node] = dist(start, node)\n",
    "    dist = {v: inf for v in list(nx.nodes(graph))} \n",
    "    # nodes we've visited\n",
    "    visited = set() \n",
    "    # prioritize nodes from start -> node with the shortest distance!\n",
    "    ## elements stored as tuples (distance, node) \n",
    "    pq = PriorityQueue()  \n",
    "    \n",
    "    dist[start] = 0  # dist from start -> start is zero\n",
    "    pq.put((dist[start], start))\n",
    "    \n",
    "    while 0 != pq.qsize():\n",
    "        curr_cost, curr = pq.get()\n",
    "        visited.add(curr)\n",
    "        #print(f'visiting {curr}')\n",
    "        # look at curr's adjacent nodes\n",
    "        for neighbor in dict(graph.adjacency()).get(curr):\n",
    "            # if we found a shorter path \n",
    "            path = dist[curr] + cost(curr, neighbor)\n",
    "            if path < dist[neighbor]:\n",
    "                # update the distance, we found a shorter one!\n",
    "                dist[neighbor] = path\n",
    "                # update the previous node to be prev on new shortest path\n",
    "                prev[neighbor] = curr\n",
    "                # if we haven't visited the neighbor\n",
    "                if neighbor not in visited:\n",
    "                    # insert into priority queue and mark as visited\n",
    "                    visited.add(neighbor)\n",
    "                    pq.put((dist[neighbor],neighbor))\n",
    "                # otherwise update the entry in the priority queue\n",
    "                else:\n",
    "                    # remove old\n",
    "                    _ = pq.get((dist[neighbor],neighbor))\n",
    "                    # insert new\n",
    "                    pq.put((dist[neighbor],neighbor))\n",
    "    print(\"=== Dijkstra's Algo Output ===\")\n",
    "    #print(\"Distances\")\n",
    "    #print(dist)\n",
    "    #print(\"Visited\")\n",
    "    #print(visited)\n",
    "    #print(\"Previous\")\n",
    "    #print(prev)\n",
    "    # we are done after every possible path has been checked \n",
    "    return backtrace(prev, start, end), dist[end]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use this newly defined dijkstra function to compute the distance between 'IND' and 'FAI'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You might find a different shortest path from the one found before (but with the same length). Indeed, by printing all the shortest paths between the two nodes we can see that, in this case, the shortest path is not unique."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print([p for p in nx.all_shortest_paths(G, 'IND', 'FAI')])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# EXERCISE 4\n",
    "\n",
    "Is it possible to travel from any airport in the US to any other airport in the US, possibly using connecting flights? In other words, does there exist a path in the network between every possible pair of airports?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, open the US flight dataset with [Gephi](https://gephi.org/). You can run gephi by entering the command `gephi` in a terminal of the IMATH servers (from a thinlinc client), or you can install it on your computer.\n",
    "\n",
    "Try different layouts and visualizations, for example the \"Geo Layout\" allows you to use plot the network following the geographical locations of each airport (you might have to install the GeoLayout plugin in Tool > Plugins). You can also detect the connected components in the \"Statistics\" panel and display them with different colors in the \"Appearance\" panel (Nodes > Partition> Component ID)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.10"
  },
  "vscode": {
   "interpreter": {
    "hash": "3699540f6baed5bd29a193b0c2d028af3f2c80498e3cac18f2b44cdd848387e2"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
